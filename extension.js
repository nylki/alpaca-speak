const St = imports.gi.St
const Main = imports.ui.main
const ml = imports.mainloop
const GLib = imports.gi.GLib
const Util = imports.misc.util
const ByteArray = imports.byteArray;
// imports.gi.versions.Gtk = '3.0'
// const Gtk = imports.gi.Gtk

let timeout
let revealer
let alpacaButton, bin

const username = GLib.get_user_name()

let espeakCmd = 'espeak-ng'

function getEspeakCmd () {
  try {
    let [res, out, err, status] = GLib.spawn_command_line_sync('espeak-ng --version')
    if(status === 0) {
      return 'espeak-ng'
    }
  } catch(e) {
  }
  
  try {
    let [res, out, err, status] = GLib.spawn_command_line_sync('espeak --version')
    if(status === 0) {
      return 'espeak'
    }
  } catch(e) {
  }

  global.console('espeak not installed (expecting espeak or espeak-ng cli to be in PATH)!')
}

function init() {
  try {
    bin = new St.Bin({
      style_class: 'panel-button'
    })
    alpacaButton = new St.Button({
      // reactive: true,
      label: '🦙'
    })

    

    bin.set_child(alpacaButton)

    alpacaButton.add_style_class_name('alpaca-button')
    alpacaButton.connect('clicked', () => {
      Util.spawn(['/bin/bash', '-c', `printf "Hi ${username}!" | ${espeakCmd} -g 20`])
    })
  } catch (e) {
    global.log('init error', e)
  }
}


function enable() {
  try {
    global.log('ENABLE')
    espeakCmd = getEspeakCmd()
    Main.panel._rightBox.insert_child_at_index(bin, 1);
    
    
    // timeout = ml.timeout_add_seconds(1.0, () => {
    //   // label.add_style_class_name('hidden-label')
    //   return true;
    // })
  } catch (e) {
    global.log('enable error', e)
  }
}

function disable() {
  global.log('DISABLE')
  Main.panel._rightBox.remove_child(bin)
  
  // ml.source_remove(timeout)
}
